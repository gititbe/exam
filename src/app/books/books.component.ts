import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { BooksService } from '../books.service';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  
  nickname;
  user = [];
  books = [];

  book:string;
  writer:string;
  read:boolean;

  addBook(){
    this.booksService.addBook(this.book ,this.writer);//שולח את הטודו שקיבלנו בעמוד
    this.book = '';//מאפס לקראת הפעם הבאה
    this.writer = '';
  }

  constructor(private db:AngularFireDatabase,
              public authService: AuthService,
              private booksService: BooksService) { }

  ngOnInit() {
    console.log("|khfbikdr");
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/nick').snapshotChanges().subscribe(
        nicks => {
          nicks.forEach(
                nick => {
                  let y = nick.payload.toJSON();
                  this.user.push(y);            
                   this.nickname = this.user[0].nickname;   
                   console.log(":"+this.nickname);      
            }
          )
          
         
        }
      )
      this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe(
        books => {
          this.books = [];
          books.forEach(
            book => {
              let b = book.payload.toJSON();
              b['key'] = book.key;
              this.books.push(b);
            }

          )
        }
      )
      
        
      }
     
    )
    
  }
}

 