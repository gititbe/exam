import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BooksService } from '../books.service';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input() data:any;//אב לבן

  book;
  writer;
  tempBook;
  tempWriter;
  key;
  read:boolean;

  showTheWriter = false;
  showEditField = false;

  showEdit()
  {
    this.tempBook = this.book;//שומרים את הטקסט בצד
    this.tempWriter = this.writer;
    this.showEditField = true;//מציגים את האפשרות לערוך
  }
  checkRead()
  {
    this.booksService.updateRead(this.key,this.read);
  }
  save()
  {
    this.booksService.updateBook(this.key,this.book, this.writer, this.read);
    this.showEditField = false;
  }

  cancel()
  {
    this.book = this.tempBook;
    this.writer = this.tempWriter;
    this.showEditField = false;
    this.showTheWriter = false;
  }

  showButton()
  {
    this.showTheWriter = true;
  }

  hideButton()
  {
    this.showTheWriter = false;
  }



  constructor(private booksService:BooksService) { }

  ngOnInit() {

    this.book = this.data.book;
    this.writer = this.data.writer;
    this.read = this.data.read;
    this.key = this.data.$key;
 }

}