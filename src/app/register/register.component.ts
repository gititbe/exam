import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  password2: string;
  name: string;
  code = '';
  message = '';
  hide = true;
  nickname ='';
  valid = false;
  validation = "";
  register()
  {
    this.authService.register(this.email,this.password)//שלחנו את המייל וססמא
    .then(value => { 
          this.authService.updateProfile(value.user,this.name);//מוסיף ליוזר את השם שלו
          this.authService.addUser(value.user,this.nickname); //שומר את היוזר בדטה בייס
    }).then(value =>{
      this.router.navigate(['/books']);// העברה לעמוד הבא
    }).catch(err => {//במידה ויש שגיאה אוטומטי יגיע לפה
      this.code = err.code;//מכניסים לתוך ארור את השגיאה שקיבלנו
      this.message = err.message;
      console.log(err);
    })
    //console.log("register " + this.name + ' ' + this.email + ' ' + this.password);
  }
  
  constructor(private router:Router,private authService:AuthService ) { }

  ngOnInit() {
  }

}
