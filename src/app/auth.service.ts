import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string)
  {
    return this.fireBaseAuth//משתנה שפתחנו בקונסטרקטור
              .auth.createUserWithEmailAndPassword(email, password);//, פונקציה שמורה של פיירבייס
  }

  updateProfile(user, name:string)
  {
    user.updateProfile({displayName:name, photoURL:''});//הפונקציה לא מחזירה תשובה
  }
  addUser(user, nickname:string)
  {
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).child('nick').push({'nickname':nickname});
  }
  logout()
  {
   return this
          .fireBaseAuth
          .auth
          .signOut();
  }
  login(email:string, password:string)
  {
    return this.
          fireBaseAuth.
          auth.
          signInWithEmailAndPassword(email,password);
  }
  

  user: Observable<firebase.User>;

  constructor(private fireBaseAuth: AngularFireAuth, 
    private db:AngularFireDatabase) 
    {
      this.user = fireBaseAuth.authState;//כדי לדעת אם היוזר מחובר או לא
    }
}
