import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  code ='';
  message ='';

  toLogin()
  {
    this.authService.login(this.email,this.password)
    .then(user =>{
      this.router.navigate(['/books']);
    }).catch(err=>{//אי אר אר אנחנו מקבלים מתוך הפונקציה שבסרוויס ולכן לא צריך להצהיר עליו כמשתנה
      this.code = err.code;
      this.message = err.message;
      console.log(err);
    })
  }
  constructor(private router:Router, private authService:AuthService) { }

  ngOnInit() {
  }

}
