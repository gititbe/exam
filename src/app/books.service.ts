import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  addBook(book:string, writer:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').push({'book':book,'writer':writer, 'read':false});
    })
  }


  updateBook(key:string,book:string, writer:string, read:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'book':book, 'writer':writer,'read':read});
    })
  }


  updateRead(key:string, read:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'read':read});
    })
    
  }
  constructor(private authService: AuthService, private db: AngularFireDatabase) { }
}